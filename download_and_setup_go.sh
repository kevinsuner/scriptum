#!/bin/bash

RED="\033[0;31m"
GREEN="\033[0;32m"
YELLOW="\033[0;33m"
BLUE="\033[0;34m"
NC="\033[0m"

ERROR="${RED}ERROR: "
SUCCESS="${GREEN}SUCCESS: "
INFO="${BLUE}INFO: "

GO_URL="https://go.dev"

# Check if Go is already installed
echo -e "${INFO}Checking if Go is installed...${NC}"
if [ -d "/usr/local/go" ]; then
    echo -e "${ERROR}Go is already installed${NC}"
    exit 0
fi

# Check if /usr/local/go is exported in .zshrc
echo -e "${INFO}Checking if Go is in path...${NC}"
if [ $(cat $HOME/.zshrc | grep "/usr/local/go" | wc -m) -gt 0 ]; then
    echo -e "${ERROR}Go is already in path${NC}"
    exit 0
fi

# Gather the host's operating system
goos=""
echo -e "${INFO}Checking OS...${NC}"
case "$OSTYPE" in
    linux*) echo -e "${SUCCESS}Detected Linux as the host's operating system${NC}"; goos="linux" ;;
    darwin*) echo -e "${SUCCESS}Detected MacOS as the host's operating system${NC}"; goos="darwin" ;;
    *) echo -e "${ERROR}Unsupported operating system${NC}"; exit 0 ;;
esac

# Gather the host's cpu architecture
goarch=""
echo -e "${INFO}Checking ARCH...${NC}"
case "$(uname -m)" in
    386) echo -e "${SUCCESS}Detected 386 as the host's cpu architecture${NC}"; goarch="386" ;;
    x86_64) echo -e "${SUCCESS}Detected x86_64 as the host's cpu architecture${NC}"; goarch="amd64" ;;
    aarch64) echo -e "${SUCCESS}Detected aarch64 as the host's cpu architecture${NC}"; goarch="arm64" ;;
    *) echo -e "${ERROR}Unsupported cpu architecture${NC}"; exit 0 ;;
esac

# Use cURL in silent mode to get the latest version of Go
echo -e "${INFO}Checking Go's latest version...${NC}"
version=$(curl -s ${GO_URL}/VERSION?m=text | head -n 1)
echo -e "${SUCCESS}Got version $version${NC}"

# Use cURL to download the latest version of Go
echo -e "${INFO}Downloading $version...${NC}"
cd "$HOME/Downloads"; $(curl -s -OL ${GO_URL}/dl/$version.$goos-$goarch.tar.gz)
if [ -f "$HOME/Downloads/$version.$goos-$goarch.tar.gz" ]; then
    echo -e "${SUCCESS}Downloaded $version.$goos-$goarch.tar.gz${NC}"
else
    echo -e "${ERROR}Failed to download $version.$goos-$goarch.tar.gz${NC}"
    exit 0
fi

# Extract the contents of the .tar.gz file into /usr/local/go
echo -e "${INFO}Extracting $version.$goos-$goarch.tar.gz into /usr/local/go... ${RED}[SUDO]${NC}"
$(sudo tar -C /usr/local -xzf "$HOME/Downloads/$version.$goos-$goarch.tar.gz")
if [ -d "/usr/local/go" ]; then
    echo -e "${SUCCESS}Extracted $version.$goos-$goarch.tar.gz into /usr/local/go${NC}"
else
    echo -e "${ERROR}Failed to extract $version.$goos-$goarch.tar.gz into /usr/local/go${NC}"
    exit 0
fi

# Remove the downloaded .tar.gz file
echo -e "${INFO}Removing $version.$goos-$goarch.tar.gz...${NC}"
$(rm -rf "$HOME/Downloads/$version.$goos-$goarch.tar.gz")
if [ ! -d "$HOME/Downloads/$version.$goos-$goarch.tar.gz" ]; then
    echo -e "${SUCCESS}Removed $version.$goos-$goarch.tar.gz${NC}"
else
    echo -e "${ERROR}Failed to remove $version.$goos-$goarch.tar.gz${NC}"
    exit 0
fi

# Export the path to the Go binary
echo -e "${INFO}Exporting path to the Go binary...${NC}"
echo 'export GO_BIN="/usr/local/go/bin"' >> $HOME/.zshrc
echo 'export PATH="$GO_BIN:$PATH"' >> $HOME/.zshrc

echo -e "${SUCCESS}Installed $version.$goos-$goarch, run ${YELLOW}source .zshrc${GREEN} to begin using it${NC}"
exit 1
